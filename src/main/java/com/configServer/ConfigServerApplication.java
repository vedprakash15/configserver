package com.configServer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@EnableConfigServer
@SpringBootApplication
public class ConfigServerApplication {


	public static void main(String[] args) {
		try {
			SpringApplication.run(ConfigServerApplication.class, args);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
