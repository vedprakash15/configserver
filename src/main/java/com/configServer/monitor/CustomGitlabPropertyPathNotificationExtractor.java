package com.configServer.monitor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.config.monitor.PropertyPathNotification;
import org.springframework.cloud.config.monitor.PropertyPathNotificationExtractor;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.util.MultiValueMap;

import java.util.*;

@Slf4j
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CustomGitlabPropertyPathNotificationExtractor implements PropertyPathNotificationExtractor {
    @Override
    public PropertyPathNotification extract(MultiValueMap<String, String> headers, Map<String, Object> payload) {
        if ("Push Hook".equals(headers.getFirst("X-Gitlav-Event"))) {
            if ((Integer) payload.get("total_commits_count") > 20) {
                System.out.println("commit in push, refressing all server ,"
                        + " " + payload.get("total_commits_count"));
                return new PropertyPathNotification(new String[]{"applications"});
            }

            if (payload.get("commits") instanceof Collections) {
                Set<String> paths = new LinkedHashSet<>();
                Collection<Map<String, Object>> commits = ((Collection<Map<String, Object>>) payload.get("commits"));
                addPaths(paths, commits);

                if (!paths.isEmpty()) {
                    System.out.println("updated applications  " +  paths);
                    return new PropertyPathNotification(paths.toArray(new String[0]));
                }
            }
        }
    return null;
    }
    protected void addPaths(Set<String> paths, Collection<Map<String, Object>> commits) {
        for (Map<String, Object> commit : commits) {
            addAllPaths(paths, commit, "added");
            addAllPaths(paths, commit, "removed");
            addAllPaths(paths, commit, "modified");
        }
    }
    private void addAllPaths(Set<String> paths, Map<String, Object> commit, String name) {
        Collection<String> files = (Collection<String>) commit.get(name);
        for(String file : files) {
            String[] fileSplits = file.split("/");
            if (fileSplits[0].equals("global")) {
                paths.add("application");
            } else {
                paths.add(fileSplits[0]);
            }
        }
    }
}
