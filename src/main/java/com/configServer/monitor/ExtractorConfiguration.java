package com.configServer.monitor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ExtractorConfiguration {

    @Bean
    public CustomGitlabPropertyPathNotificationExtractor gitlabPathExtractor() {
        return new CustomGitlabPropertyPathNotificationExtractor();
    }
}
